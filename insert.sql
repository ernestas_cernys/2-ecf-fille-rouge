/*==============================================================*/
/* Project: Fil Rouge « Active »                                */
/* Author: Ernestas Cernys										*/
/* Version: final							Date: 14/02/2020	*/
/* 																*/
/*==============================================================*/

/*==============================================================*/
/* 					Insert data				                    */
/*==============================================================*/


SET FOREIGN_KEY_CHECKS=0;

truncate `Intervention_technical_information`;
truncate `Project_technical_information`;
truncate `Intervention_document`;
truncate `Stage_document`;
truncate `Project_document`;
truncate `Technical_information`;
truncate `User`;
truncate `Role_permission`;
truncate `Role`;
truncate `Permission`;
truncate `Intervention`;
truncate `Stage`;
truncate `Stage_type`;
truncate `Project`;
truncate `Sector`;
truncate `External_worker`;
truncate `Document`;
truncate `Contract`;
truncate `Status`;
truncate `Worker`;
truncate `Client`;
truncate `Function`;
truncate `Domain`;
truncate `Activity`;

SET FOREIGN_KEY_CHECKS=1;

/*==============================================================*/
/* Insert into table: Activity                                  */
/*==============================================================*/
INSERT INTO `Activity` (activity_label) VALUES
('analyse des besoins'),
('Conception'),
('Programmation'),
('Tests unitaires'),
('Tests d’intégration'),
('Tests de recette et installation'),
('Management du projet'),
('Gestion de configuration'),
('Formation spécifique au projet'),
('Divers');

/*==============================================================*/
/* Insert into table: Domain		                            */
/*==============================================================*/
INSERT INTO `Domain` (domain_label) VALUES
('Agroalimentaire'),
('Assurance'),
('Banque'),
('Textile'),
('Service aux entreprises'),
('BTP'),
('Informatique / Télécoms'),
('Transport Logistique'),
('Etudes et Conseils'),
('Multimédia'),
('Communication'),
('Commerce'),
('Grande Distribution'),
('Chimie');

/*==============================================================*/
/* Insert into table: Function                                  */
/*==============================================================*/
INSERT INTO `Function` (function_label) VALUES
('Directeur Général'),
('Directeur Administratif et Financier'),
('Responsable d\'études'),
('Responsable Ressources Humaines'),
('Responsable Commercial'),
('Commercial'),
('Secrétaire Administratif'),
('Secrétaire Technique'),
('Technico-commercial'),
('Secrétaire'),
('Responsable de Projet'),
('Analyste responsable d\'application'),
('Développeur'),
('Technicien de support');

/*==============================================================*/
/* Insert into table: Client                                    */
/*==============================================================*/
INSERT INTO `Client` (domain_id, client_society_label, client_type, client_nature, client_address, client_phone_number, client_revenus, general_comment, client_worker_count) VALUES
(9,"Vulputate Dui Nec Corp.","Public","Ancienne","Appartement 308-720 Libero. Ave, BiercŽe, 15123",0407949601,426530,"interdum. Nunc sollicitudin commodo", 10),
(1,"Commodo At Consulting","Public","Ancienne","6965 Purus Impasse, Hattiesburg, 88390",0203988096,511543,"erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum", 100),
(3,"Mollis Nec Institute","Public","Secondaire","402-4137 Magnis Avenue, Bima, 56665",0606767757,264832,"mauris sit amet lorem semper auctor. Mauris vel turpis. Aliquam", 200),
(8,"Gravida Sagittis Duis Institute","Public","Secondaire","666-5782 Arcu. Route, Gibsons, 47445",0758397056,132131,"Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque",150),
(8,"In Faucibus Limited","Privé","Secondaire","CP 678, 4050 Pede. Avenue, Grafton, 06287", 0648651931 ,171191,"Nullam, sodales elit erat vitae risus. Duis a mi",156),
(3,"Non Lorem Inc.","Privé","Ancienne","CP 439, 3067 Venenatis Rue, Hyères, 617544",0728750421,188229,"magna sed dui. Fusce aliquam, enim nec tempus scelerisque",1235),
(13,"Gravida Associates","Privé","Ancienne","8811 Feugiat Rd., Huesca, 350990",0678665125,113271,"sit amet",54),
(7,"Nulla Cras Incorporated","Public","Principale","Appartement 571-6983 Sem Chemin, Salem, 80540",0444211154,943145,"mollis",46),
(8,"Tincidunt LLC","Public","Ancienne","CP 410, 5653 Dictum Route, Ansfelden, 6091",0656385000, 624139,"elit. Aliquam auctor, velit eget laoree",465),
(4,"Auctor Velit Corporation","Privé","Ancienne","Appartement 239-8067 Quam Rue, Hampstead, 78662",0498243951, 197642,"dui, in t posuere, enim nisl elementum",654);

/*==============================================================*/
/* Insert into table: Worker                                    */
/*==============================================================*/
INSERT INTO `Worker` (worker_full_name, worker_sex, worker_civility, worker_address1, worker_city, worker_postalcode, worker_phone_number) VALUES
("Norman Mercer","F","Madame","CP 265, 6067 Augue Chemin","Savannah","05374","0499296584"),
("Nayda Randolph","F","Mademoiselle","474-6988 Mauris Impasse","Ingolstadt","11704","0790419309"),
("Idola Kelley","F","Madame","CP 337, 423 Ipsum. Ave","Joliet","13069","0240913468"),
("Keaton Butler","M","Monsieur","887-9004 Dui. Rue","Biała Podlaska","34809","0870282346"),
("Mason Kent","M","Monsieur","2686 Netus Avenue","Talara","5878","0916157013"),
("Rhiannon Mitchell","F","Mademoiselle","CP 430, 1261 Consequat Impasse","Killa Abdullah","5110","0218566893"),
("Bert Bray","M","Monsieur","CP 646, 897 Habitant Ave","Lehri","4824","0495512286"),
("Fallon Kidd","M","Monsieur","802 Ligula. Chemin","Rodengo/Rodeneck","651892","0887328195"),
("Dean Miller","M","Monsieur","588-393 Sollicitudin Chemin","Wolfsberg","R45 4BD","0224064823"),
("Alan Cash","M","Monsieur","Appartement 349-9965 Sollicitudin Avenue","Assebroek","61614","0311553365"),
("Mia Anthony","F","Mademoiselle","101 Sed Av.","Hannut","Z2369","0487584664"),
("Eric Burton","M","Monsieur","Appartement 404-8322 Vestibulum Rue","Paredones","3338","0490929454"),
("Suki Mckay","F","Madame","5932 Nulla. Ave","Ghislarengo","84317","0348863526"),
("Odysseus Harris","M","Monsieur","666-2957 Magnis Avenue","Fermont","8300","0152279369");

/*==============================================================*/
/* Insert into table: Status                                    */
/*==============================================================*/
INSERT INTO `Status` (status_id, status_label) VALUES
('CDI','Contrat à durée indéterminée'),
('CDD','Contact à durée déterminée'),
('STA','Stagiaire');

/*==============================================================*/
/* Insert into table: Contract                                  */
/*==============================================================*/
INSERT INTO `Contract` (status_id, function_id, worker_id, contract_start_date, contract_end_date, worker_salary) VALUES
("STA",10,1,"2020-10-08",null,3511),
("STA",7,2,"2020-09-16",null,6497),
("CDI",10,3,"2019-05-23",null,2774),
("CDD",6,4,"2019-09-15",null,6782),
("CDD",1,5,"2019-10-14",null,6342),
("CDD",9,6,"2019-10-24","2020-09-29",3040),
("CDD",3,6,"2020-10-01",null,2931),
("STA",2,7,"2019-06-20","2019-09-23",5869),
("STA",8,7,"2020-09-28",null,3381),
("STA",8,8,"2019-07-19","2019-10-10",3368),
("STA",10,8,"2019-11-10",null,4625),
("STA",1,9,"2019-06-19","2019-07-05", 7868),
("CDD",7,9,"2019-08-18","2020-05-13",3642),
("CDD",11,9,"2020-11-04",null,8784),
("CDI",7,10,"2020-06-17","2020-12-05",8389),
("CDI",1,10,"2020-12-06",null,5388),
("STA",13,11,"2020-03-18",null,6033),
("STA",12,12,"2019-05-27","2019-12-27",9213),
("CDD",4,12,"2019-12-28",null,3449),
("STA",4,13,"2019-12-17","2020-01-20",9950),
("CDD",6,13,"2020-01-21",null,2804),
("CDD",14,14,"2020-06-23",null,9519);

/*==============================================================*/
/* Insert into table: Document                                  */
/*==============================================================*/
INSERT INTO `Document` (worker_id, document_title, document_summary, document_release_date) VALUES
(3,"nulla vulputate dui, nec tempus","placerat velit. Quisque varius. Nam porttitor scelerisque","2019-08-28"),
(13,"sit amet ante. Vivamus non","Curabitur vel lectus. Cum sociis","2019-05-27"),
(1,"eu arcu. Morbi sit amet","sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum","2019-08-08"),
(9,"mi. Aliquam gravida mauris ut","nec enim. Nunc ut erat. Sed nunc est, mollis","2019-08-14"),
(2,"nunc nulla vulputate dui, nec","cursus et, magna. Praesent interdum","2019-06-18"),
(2,"elit, pellentesque a, facilisis non,","sed orci lobortis augue scelerisque mollis. Phasellus libero mauris,","2019-12-23"),
(6,"dolor elit, pellentesque a, facilisis","a odio semper cursus. Integer mollis. Integer tincidunt aliquam","2019-05-04"),
(14,"lobortis ultrices. Vivamus rhoncus. Donec","ante blandit viverra. Donec tempus, lorem fringilla ornare placerat,","2019-11-24"),
(3,"velit. Pellentesque ultricies dignissim lacus.","eu sem. Pellentesque ut ipsum","2019-07-05"),
(7,"elit erat vitae risus. Duis","Donec elementum, lorem ut aliquam iaculis, lacus pede sagittis","2019-07-11"),
(5,"vel, faucibus id, libero. Donec","Curabitur consequat, lectus sit amet luctus vulputate,","2019-03-27"),
(5,"Etiam bibendum fermentum metus. Aenean","dui augue eu tellus. Phasellus elit pede, malesuada vel,","2019-04-16"),
(2,"tincidunt, nunc ac mattis ornare,","Aliquam fringilla cursus purus. Nullam","2019-11-23"),
(9,"dapibus ligula. Aliquam erat volutpat.","dignissim pharetra. Nam ac nulla. In tincidunt congue turpis.","2019-06-20"),
(6,"sodales elit erat vitae risus.","sed leo. Cras vehicula aliquet libero.","2019-06-01"),
(1,"fermentum arcu. Vestibulum ante ipsum","consectetuer mauris id sapien. Cras dolor dolor,","2019-11-16"),
(4,"egestas rhoncus. Proin nisl sem,","neque pellentesque massa lobortis ultrices.","2019-05-25"),
(11,"euismod ac, fermentum vel, mauris.","mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla","2019-04-04"),
(3,"lectus quis massa. Mauris vestibulum,","eleifend. Cras sed leo. Cras vehicula aliquet","2019-04-10"),
(4,"felis. Donec tempor, est ac","et ultrices posuere cubilia Curae; Donec tincidunt. Donec","2019-04-01"),
(13,"laoreet posuere, enim nisl elementum","pede ac urna. Ut tincidunt vehicula risus. Nulla eget","2019-08-28"),
(14,"non massa non ante bibendum","Cum sociis natoque penatibus et magnis dis","2019-07-13"),
(2,"turpis non enim. Mauris quis","lobortis quam a felis ullamcorper viverra.","2019-10-23"),
(8,"aliquet magna a neque. Nullam","nunc sit amet metus. Aliquam erat volutpat. Nulla","2019-08-13"),
(4,"a neque. Nullam ut nisi","vulputate, lacus. Cras interdum. Nunc sollicitudin","2019-12-02"),
(9,"nascetur ridiculus mus. Aenean eget","Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare,","2019-07-05"),
(3,"rutrum eu, ultrices sit amet,","elementum sem, vitae aliquam eros","2019-06-09"),
(12,"et magnis dis parturient montes,","felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed","2019-02-18"),
(14,"Nulla facilisis. Suspendisse commodo tincidunt","Aliquam gravida mauris ut mi.","2019-10-18"),
(10,"mollis dui, in sodales elit","nec tempus scelerisque, lorem ipsum sodales purus,","2020-01-23"),
(9,"dolor. Fusce mi lorem, vehicula","Vivamus rhoncus. Donec est. Nunc ullamcorper,","2019-04-28"),
(7,"ut, pellentesque eget, dictum placerat,","nisl. Quisque fringilla euismod enim. Etiam","2019-08-17"),
(9,"mauris sit amet lorem semper","quis lectus. Nullam suscipit, est ac","2019-02-26"),
(11,"ullamcorper eu, euismod ac, fermentum","Donec consectetuer mauris id sapien.","2019-05-03"),
(12,"lorem, eget mollis lectus pede","Quisque purus sapien, gravida non, sollicitudin a, malesuada","2019-11-29"),
(14,"luctus, ipsum leo elementum sem,","malesuada. Integer id magna et ipsum cursus vestibulum.","2019-12-22"),
(9,"Fusce aliquam, enim nec tempus","Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci","2019-07-03"),
(13,"tempor diam dictum sapien. Aenean","sed orci lobortis augue scelerisque mollis.","2019-06-27"),
(12,"commodo at, libero. Morbi accumsan","turpis egestas. Aliquam fringilla cursus","2019-07-04"),
(11,"Cras dictum ultricies ligula. Nullam","consectetuer rhoncus. Nullam velit dui, semper","2019-05-15");  

/*==============================================================*/
/* Insert into table: External_worker                           */
/*==============================================================*/
INSERT INTO `External_worker` (client_id, function_id, external_worker_fullname, external_worker_sex, external_worker_civility, external_worker_phone_number) VALUES 
(2,6,"Yasir Shaw","F","Mademoiselle","0854188611"),
(6,9,"Michaele Carroll","F","Mademoiselle","0592449269"),
(6,8,"Ciaran Bender","M","Monsieur","0907351665"),
(5,10,"Sandra Buck","F","Madame","0940819499"),
(1,10,"Althea Larson","F","Madame","0243745158"),
(9,3,"Martin Fuller","M","Monsieur","0645977593"),
(6,10,"Raymond Clayton","M","Monsieur","0960991768"),
(6,2,"Coby Schneider","M","Monsieur","0250045160"),
(1,4,"Maia Washington","F","Madame","0314720345"),
(1,5,"Nygel Maynard","M","Monsieur","0297224969");

/*==============================================================*/
/* Insert into table: Sector                                    */
/*==============================================================*/
INSERT INTO `Sector` (sector_label) VALUES
('Gestion Commercial'),
('RH'),
('Production'),
('Achat'),
('Gestion Logistique'),
('Gestion Ticketing'),
('Gestion Support');

/*==============================================================*/
/* Insert into table: Project                                   */
/*==============================================================*/
INSERT INTO `Project` (project_id, client_id, external_worker_id, sector_id, worker_id, project_short_label, 
    project_label, project_type, project_planed_start_date, project_planed_end_date, max_worker_count, 
    project_estimated_workload, project_workload_comment, project_code_length, project_client_comment) VALUES
(1910,7,7,3,1,"dolor","a, magna. Lorem","Régie","2019-01-15","2020-08-20",16,36472,"ut, pharetra sed, hendrerit a, arcu. Sed et",95777,"mi tempor lorem, eget mollis"),
(1901,10,1,1,5,"ultricies","sagittis felis. Donec","Assistance","2019-12-13","2020-09-20",25,11934,"at augue id ante dictum",76510,"ac orci. Ut semper pretium neque."),
(1902,9,8,5,3,"Sed","amet luctus","Régie","2019-08-28","2020-11-21",10,95418,"Donec fringilla. Donec feugiat metus sit amet ante.",67582,"felis. Donec tempor, est ac"),
(1903,10,6,1,9,"fringilla,","odio","Régie","2019-07-24","2020-02-11",24,80084,"dictum placerat, augue. Sed molestie. Sed id risus quis",59667,"Nunc ullamcorper, velit in aliquet lobortis,"),
(1904,8,6,2,6,"erat","montes, nascetur","Forfait","2019-09-12","2020-06-01",10,16902,"Cras lorem lorem, luctus ut, pellentesque eget,",91704,"auctor odio a purus. Duis elementum, dui quis accumsan"),
(1905,8,3,7,8,"odio.","in consectetuer ipsum nunc","Régie","2019-01-14","2020-06-09",22,64153,"metus urna convallis erat, eget tincidunt dui augue",75944,"libero. Donec consectetuer mauris id sapien."),
(1906,3,7,5,10,"ligula","Proin","Forfait","2019-11-13","2020-12-02",16,35848,"facilisi. Sed neque.",75773,"erat nonummy ultricies ornare, elit elit fermentum"),
(1907,7,8,4,9,"Aliquam","urna. Ut tincidunt","Assistance","2019-01-16","2020-11-06",20,55123,"Nunc ullamcorper, velit in aliquet",85954,"ac, feugiat non, lobortis quis, pede. Suspendisse dui. Fusce"),
(1908,1,8,6,3,"ante.","primis in faucibus orci","Forfait","2019-04-28","2020-01-20",18,85991,"molestie sodales. Mauris blandit enim consequat purus.",59296,"Vivamus molestie dapibus ligula. Aliquam erat"),
(1909,7,8,2,4,"non,","dui lectus rutrum","Forfait","2019-02-03","2021-01-21",21,11001,"Nulla aliquet. Proin velit. Sed malesuada augue ut lacus.",85968,"Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere");

/*==============================================================*/
/* Insert into table: Stage_type                                */
/*==============================================================*/
INSERT INTO `Stage_type` (stage_type_label) VALUES
('Etudes de l\'existant'),
('Développement'),
('Tests avant mise en production'),
('Mise en Production'),
('Maintenance');

/*==============================================================*/
/* Insert into table: Stage                                     */
/*==============================================================*/
INSERT INTO `Stage` (project_id, stage_type_id, stage_validation_workload, stage_planned_workload) VALUES
(1901,5,111,12093), (1901,4,165,13224), (1902,5,166,14401), (1902,3,117,13186),
(1902,4,156,7268), (1902,2,114,12161), (1902,1,180,7916), (1901,2,115,11157),
(1901,3,132,7100), (1903,5,132,11368), (1903,2,191,8756), (1903,1,117,5847),
(1903,3,158,13099), (1903,4,142,12290), (1904,4,176,8722), (1904,5,192,9188),
(1904,3,114,11883), (1904,2,162,5488), (1905,1,138,12456), (1905,2,106,9918),
(1905,4,199,8026), (1905,3,139,12612), (1905,5,130,5512), (1906,1,100,10254),
(1906,2,189,13507), (1906,3,157,6160), (1906,4,134,10015), (1906,5,157,14786),
(1907,1,162,12433), (1907,2,166,14956), (1907,3,196,12197), (1908,1,150,11316),
(1908,2,109,12252), (1908,3,135,12004), (1908,4,168,10587), (1908,5,126,6902),
(1909,1,179,5668), (1909,2,148,14425), (1909,3,152,7379), (1909,4,171,11933),
(1909,5,104,14206), (1910,1,126,13321), (1910,2,193,13569);

/*==============================================================*/
/* Insert into table: Intervention                              */
/*==============================================================*/
INSERT INTO `Intervention` (stage_id, activity_id, worker_id, external_worker_id, function_id, intervention_date, intervention_workload) VALUES
(14,2,NULL,5,14,'2020-02-02',7),(38,7,9,NULL,13,'2019-10-25',4),(32,4,NULL,8,12,'2019-07-27',8),(16,6,6,NULL,13,'2019-09-28',8),(19,7,NULL,3,14,'2019-04-27',2),
(5,4,5,NULL,14,'2019-07-23',5),(26,10,NULL,5,14,'2019-07-15',4),(24,6,NULL,4,14,'2019-11-27',6),(24,10,4,NULL,14,'2019-10-25',4),(32,8,NULL,10,13,'2019-09-23',4),
(8,5,4,NULL,13,'2019-04-10',2),(2,4,NULL,7,14,'2019-05-31',3),(12,6,4,NULL,13,'2019-03-18',4),(9,9,NULL,2,12,'2019-09-02',3),(8,8,12,NULL,12,'2019-03-05',1),
(41,7,NULL,6,13,'2020-01-05',7),(15,3,14,NULL,13,'2019-09-17',2),(21,10,NULL,3,14,'2019-12-06',6),(27,10,9,NULL,13,'2019-04-03',8),(6,10,7,NULL,13,'2019-07-30',7),
(39,6,NULL,9,13,'2019-11-22',4),(31,6,NULL,8,13,'2019-09-30',4),(17,6,8,NULL,12,'2019-10-23',6),(16,2,9,NULL,12,'2019-10-22',2),(23,4,NULL,8,13,'2019-09-23',2),
(35,8,NULL,4,12,'2019-08-07',1),(22,6,8,NULL,13,'2019-11-29',7),(42,9,1,NULL,13,'2019-04-03',6),(4,2,NULL,2,14,'2019-10-27',6),(14,4,NULL,6,13,'2020-01-01',4),
(29,4,6,NULL,12,'2019-10-06',2),(19,5,10,NULL,12,'2019-09-24',3),(40,5,NULL,9,13,'2019-06-15',4),(30,3,NULL,5,12,'2019-07-04',7),(38,3,13,NULL,13,'2020-01-14',5),
(17,10,13,NULL,12,'2019-12-01',2),(41,6,NULL,3,14,'2019-05-27',5),(3,8,NULL,1,13,'2019-07-14',3),(14,3,14,NULL,14,'2019-04-01',7),(15,5,14,NULL,14,'2019-05-22',7),
(28,8,NULL,3,13,'2019-03-22',8),(41,6,NULL,10,14,'2019-10-27',7),(31,8,4,NULL,12,'2019-02-09',4),(36,8,6,NULL,14,'2019-12-07',2),(17,7,NULL,2,12,'2019-07-16',7),
(33,9,NULL,8,12,'2019-12-29',5),(19,2,3,NULL,14,'2019-08-22',5),(17,5,11,NULL,13,'2019-10-31',3),(18,7,NULL,6,13,'2019-08-31',6),(22,6,NULL,1,14,'2019-06-28',5),
(12,5,NULL,2,13,'2019-02-09',8),(14,7,14,NULL,12,'2019-11-11',2),(30,7,7,NULL,14,'2019-12-22',7),(30,1,11,NULL,14,'2019-05-03',5),(9,1,2,NULL,14,'2019-03-15',6),
(26,10,7,NULL,14,'2019-10-25',8),(29,4,6,NULL,14,'2019-02-22',7),(17,4,10,NULL,12,'2019-06-18',4),(3,5,NULL,4,14,'2019-03-19',1),(41,5,NULL,5,14,'2019-10-02',1),
(36,1,NULL,6,12,'2019-05-21',5),(29,1,NULL,10,13,'2019-05-02',8),(26,4,NULL,8,14,'2019-10-18',7),(24,7,NULL,2,13,'2019-07-09',8),(34,5,NULL,8,13,'2019-06-21',4),
(41,10,NULL,9,14,'2019-09-15',5),(18,8,4,NULL,14,'2020-01-16',7),(17,5,6,NULL,14,'2019-12-25',1),(13,5,12,NULL,12,'2020-01-06',7),(24,8,13,NULL,14,'2019-11-25',4),
(30,3,13,NULL,13,'2019-11-26',6),(13,8,6,NULL,13,'2019-04-21',8),(23,2,4,NULL,14,'2019-11-23',5),(28,4,5,NULL,13,'2019-10-14',6),(23,10,3,NULL,13,'2019-11-23',8),
(8,8,11,NULL,12,'2019-07-20',8),(34,6,NULL,9,12,'2019-12-12',8),(28,1,NULL,9,12,'2019-05-09',5),(3,8,NULL,6,12,'2019-10-09',3),(7,7,NULL,3,12,'2020-01-22',7),
(38,2,NULL,4,12,'2019-07-14',1),(13,5,NULL,2,14,'2019-12-23',8),(35,5,NULL,8,12,'2019-11-17',3),(25,6,10,NULL,12,'2019-02-12',6),(22,7,6,NULL,12,'2019-04-13',7),
(9,8,9,NULL,13,'2019-03-20',5),(19,9,11,NULL,12,'2019-03-13',1),(23,5,7,NULL,13,'2019-12-06',2),(14,8,6,NULL,12,'2019-09-05',3),(3,6,10,NULL,12,'2019-09-04',6),
(22,5,10,NULL,12,'2019-12-11',7),(18,8,NULL,8,13,'2019-03-14',7),(21,5,NULL,4,13,'2019-04-07',3),(1,8,NULL,7,13,'2019-04-21',3),(25,8,NULL,4,13,'2019-04-28',1),
(42,8,NULL,3,12,'2019-11-10',4),(7,1,13,NULL,14,'2019-09-01',4),(8,4,4,NULL,14,'2019-05-08',8),(39,6,4,NULL,14,'2019-02-24',7),(42,2,11,NULL,13,'2019-08-02',7);

/*==============================================================*/
/* Insert into table: Permission                                */
/*==============================================================*/
INSERT INTO `Permission` (`key`, description) VALUES 
('manage_HR','Gestion de resources humaines'),
('manage_project','Gestion de projets, differents étapes et interventions'),
('manage_intervention','Gestion de donnés d\'interventions : charges journalièrs etc.'),
('manage_client','Gestion commerciale de clients et collaborateurs externes'),
('manage_DB','Gestion de la base de donnés : permissions, creation d\'utilisateurs etc.');    

/*==============================================================*/
/* Insert into table: Role                                      */
/*==============================================================*/
INSERT INTO `Role` (role_label) VALUES
('Administrateur'),
('Responsable d\'études'),
('Responsable commercial'),
('Chef de projet'),
('Secretaire technique'),
('Responsable RH');

/*==============================================================*/
/* Insert into table: Role_permission                           */
/*==============================================================*/
INSERT INTO `Role_permission` (role_id, `key`) VALUES
(1,'manage_HR'),
(1,'manage_project'),
(1,'manage_interventions'),
(1,'manage_client'),
(1,'manage_DB'),
(2,'manage_project'),
(3,'manage_client'),
(4,'manage_project'),
(5,'manage_intervention'),
(6,'manage_HR');

/*==============================================================*/
/* Insert into table: User                                      */
/*==============================================================*/
INSERT INTO `User` (worker_id, `login`, role_id, `password`) VALUES
(5, 'smurf145', 4, 'lkdsj546hgsbgslkdfj15gkjbgdsjg'),
(11,'DevOps1998' , 2, 'jlshg646sdgs468shssdgeeryr1324'),
(3, 'destroyer2000', 3, 'gsgersserghsd546s6sgdsgsg46564'),
(14, 'NeverGiveUp', 5, 'sdffgdsg654dsgfdsfgf8s6dfg4fds'),
(9, 'upsideDown', 6, 'sggdgsdg654dsfg654gdsgds1317sd');

/*==============================================================*/
/* Insert into table: Technical_information                     */
/*==============================================================*/
INSERT INTO `Technical_information` (technical_info_label) VALUES
('JAVA'), ('PHP'), ('JavaScript'), ('MySQL'), ('PostGreSQL'),
('Windows'), ('linux'), ('C#'), ('Delphi'), ( 'Oracle'),
( 'MySQL'), ( 'PostGree'), ( 'IBM'), ( 'MongoDB');

/*==============================================================*/
/* Insert into table: Project_document                          */
/*==============================================================*/
INSERT INTO `Project_document` (document_id, project_id) VALUES
(16,1905),(3,1905),(38,1907),(20,1901),(14,1907),(14,1910),(26,1910),(5,1902),(11,1907),(23,1906),
(28,1906),(25,1909),(13,1909),(39,1907),(3,1907),(38,1908),(4,1906),(8,1906),(30,1906),(25,1901);

/*==============================================================*/
/* Insert into table: Stage_document                            */
/*==============================================================*/
INSERT INTO `Stage_document` (document_id, stage_id) VALUES
(21,24),(3,28),(20,25),(9,13),(9,39),(6,24),(3,5),(37,28),(26,11),
(8,2),(39,4),(17,8),(10,3),(12,16),(34,42),(25,9),(18,30),(20,26),(2,18),
(8,20),(1,40),(24,20),(34,11),(4,26),(23,9),(39,26),(24,35),(13,21),(23,16),
(29,28),(9,4),(38,36),(25,17),(38,16),(13,37),(1,4),(9,19),(2,14),
(10,34),(23,32),(24,29),(32,32),(33,8),(20,15),(9,22),(38,35),(29,19),(36,4),
(37,30),(23,33),(39,27),(8,35),(7,12),(4,27),(16,32),(11,17),(1,12),(30,22),
(20,24),(33,42),(4,17),(34,3),(35,13),(2,13),(23,30),(23,19),(30,16),(5,30),
(3,1),(6,28),(7,6),(16,29),(18,14),(26,34),(28,23),(11,42),(1,5),
(12,33),(6,29),(3,12),(4,23),(18,24),(27,31),(8,10),(33,27),(22,31),(38,4),
(7,42),(10,8),(12,28),(28,27),(33,25),(27,18),(16,17),(14,39),(19,1),(36,40);

/*==============================================================*/
/* Insert into table: Intervention_document                     */
/*==============================================================*/
INSERT INTO `Intervention_document` (document_id, intervention_id) VALUES
(17,42),(11,84),(33,30),(25,61),(16,78),(10,83),(38,53),(14,97),(22,8),(5,76),
(29,81),(9,15),(22,48),(21,29),(8,41),(11,73),(29,69),(2,56),(32,10),(14,36),
(28,76),(31,36),(4,57),(9,43),(20,56),(23,27),(37,28),(39,84),(4,45),(34,8),
(37,8),(13,92),(7,76),(27,76),(27,16),(4,44),(21,43),(22,1),(6,60),(3,13),
(29,60),(34,55),(17,56),(32,64),(8,33),(26,78),(30,94),(5,58),(33,61),(37,26),
(15,58),(34,42),(26,29),(22,2),(15,59),(27,79),(2,13),(10,38),(38,51),(20,41),
(3,42),(38,13),(3,93),(25,14),(11,86),(23,21),(31,12),(20,63),(8,78),
(32,17),(9,14),(32,73),(30,92),(8,76),(29,74),(31,66),(25,24),(35,53),(34,16),
(27,22),(7,75),(10,10),(9,25),(18,41),(11,54),(10,62),(35,23),(18,52),(37,43),
(25,84),(28,42),(24,44),(3,83),(28,67),(36,65),(38,41),(2,85),(30,49),(30,60);

/*==============================================================*/
/* Insert into table: Project_technical_information             */
/*==============================================================*/
INSERT INTO `Project_technical_information` (technical_info_id, project_id) VALUES
(5,1902),(12,1901),(6,1909),(8,1903),(9,1908),(9,1904),(9,1902),(3,1903),(4,1901),
(2,1906),(12,1905),(14,1908),(7,1904),(7,1906),(8,1902),(14,1901),(8,1901),(4,1904),
(12,1906),(10,1904),(12,1902),(4,1909),(5,1907),(12,1910),(10,1901),(10,1905);

/*==============================================================*/
/* Insert into table: Intervention_technical_information        */
/*==============================================================*/
INSERT INTO `Intervention_technical_information` (technical_info_id, intervention_id) VALUES
(13,44),(2,51),(8,41),(8,86),(4,94),(11,82),(7,80),(7,96),(4,9),
(13,99),(5,58),(5,95),(10,46),(7,99),(2,14),(8,62),(9,31),(12,22),
(6,23),(2,23),(10,35),(9,30),(10,41),(8,65),(7,67),(12,55),(4,5),(8,34),
(6,50),(8,37),(3,98),(10,27),(11,49),(5,63),(14,58),(13,49),(10,87),(13,58),
(8,81),(7,19),(5,82),(10,23),(6,2),(7,42),(4,57),(11,28),(7,62),
(10,10),(10,6),(9,49),(2,76),(12,2),(14,82),(3,71),(13,48),(10,60),
(8,3),(10,40),(12,51),(13,50),(7,32),(1,2),(12,33),(10,13),(10,14),(6,19),
(5,75),(10,86),(1,90),(10,61),(13,10),(10,37),(11,66),(5,31),(8,68),(10,1),
(7,1),(1,100),(3,64),(11,37),(12,72),(9,63),(5,90),(10,93),(2,36),(1,63),
(13,83),(14,34),(10,82),(12,65),(11,39),(6,7),(7,73),(9,41),(2,70),(9,65),
(14,83),(3,50),(1,42),(13,13),(11,88),(12,63),(1,81),(4,51),(14,45),(5,79),
(12,54),(8,96),(8,55),(8,11),(2,3),(12,61),(1,73),(8,57),(6,27),(5,97),
(13,67),(8,17),(2,53),(3,89),(7,89),(4,24),(8,14),(2,80),(14,76),(1,52),
(9,27),(4,40),(9,94),(9,84),(7,90),(12,82),(1,98),(3,97),(6,52),(6,100),
(13,68),(4,73),(9,51),(1,14),(14,61),(9,21),(13,21),(5,10),(6,54),(4,52),(3,93),(7,83),(2,34),(2,21),(7,78),
(2,41),(14,11),(10,52),(14,33),(12,36),(14,15),(2,18),(10,71),(3,85),(4,14),
(6,8),(5,91),(2,56),(11,47),(3,31),(6,4),(4,7),(6,47),(8,82),
(5,25),(3,86),(8,97),(12,48),(1,64),(10,90),(5,94),(14,44),(6,38),(2,6),
(1,96),(9,24),(12,89),(8,45),(13,42),(7,98),(6,77),(11,71),(10,96),(9,1);

/*==============================================================*/
/* Data change for testing					                    */
/*==============================================================*/
INSERT INTO `project` (`project_id`, `client_id`, `external_worker_id`, `sector_id`, `worker_id`, `project_short_label`, `project_label`, `project_type`, `project_planed_start_date`, `project_planed_end_date`) VALUES ('1911', '1', '1', '1', '1', 'ds', 'dsfds', 'Forfait', '2019-02-15', '2019-12-12');
INSERT INTO `stage` (`stage_id`, `project_id`, `stage_type_id`, `stage_planned_workload`) VALUES ('44', '1911', '1', '456');
INSERT INTO `project` (`project_id`, `client_id`, `external_worker_id`, `sector_id`, `worker_id`, `project_short_label`, `project_label`, `project_type`, `project_planed_start_date`, `project_planed_end_date`, `max_worker_count`, `project_estimated_workload`) VALUES ('1912', '1', '1', '1', '1', 'lola', 'lola run', 'Régie', '2019-05-16', '2020-05-12', '25', '64564');
UPDATE `stage` SET `stage_validation_workload` = NULL WHERE (`stage_id` = '7');
UPDATE `stage` SET `stage_validation_workload` = NULL WHERE (`stage_id` = '13');
UPDATE `stage` SET `stage_validation_workload` = NULL WHERE (`stage_id` = '10');
UPDATE `contract` SET `contract_start_date` = '2010-12-17' WHERE (`contract_id` = '20');


UPDATE `contract` SET `contract_start_date` = '2010-06-23' WHERE (`contract_id` = '22');
UPDATE `contract` SET `contract_start_date` = '2000-10-08', `contract_end_date` = '2010-09-06' WHERE (`contract_id` = '1');

UPDATE `project_technical_information` SET `technical_info_id` = '1' WHERE (`technical_info_id` = '8') and (`project_id` = '1903');
UPDATE `project_technical_information` SET `technical_info_id` = '1' WHERE (`technical_info_id` = '9') and (`project_id` = '1904');
