/*==============================================================*/
/* Project: Fil Rouge « Active »                                */
/* Author: Ernestas Cernys										*/
/* Version: final							Date: 14/02/2020	*/
/* 																*/
/*==============================================================*/

/*==============================================================*/
/* 					Schema and table creation                   */
/*==============================================================*/

drop schema if exists fil_rouge;
create schema fil_rouge;
use fil_rouge;
/*
drop table if exists `Intervention_technical_information`;
drop table if exists `Project_technical_information`;
drop table if exists `Intervention_document`;
drop table if exists `Stage_document`;
drop table if exists `Project_document`;
drop table if exists `Technical_information`;
drop table if exists `User`;
drop table if exists `Role_permission`;
drop table if exists `Role`;
drop table if exists `Permission`;
drop table if exists `Intervention`;
drop table if exists `Stage`;
drop table if exists `Stage_type`;
drop table if exists `Project`;
drop table if exists `Sector`;
drop table if exists `External_worker`;
drop table if exists `Document`;
drop table if exists `Contract`;
drop table if exists `Status`;
drop table if exists `Worker`;
drop table if exists `Client`;
drop table if exists `Function`;
drop table if exists `Domain`;
drop table if exists `Activity`;
*/
/*==============================================================*/
/* Table: Activity                                              */
/*==============================================================*/
CREATE TABLE `Activity` (
    activity_id INTEGER NOT NULL AUTO_INCREMENT,
    activity_label VARCHAR(250) NOT NULL,
    CONSTRAINT PK_ACTIVITY PRIMARY KEY (activity_id)
);

/*==============================================================*/
/* Table: Domain                                                */
/*==============================================================*/
CREATE TABLE `Domain` (
    domain_id INTEGER NOT NULL AUTO_INCREMENT,
    domain_label VARCHAR(250) NOT NULL,
    CONSTRAINT PK_DOMAIN PRIMARY KEY (domain_id)
);

/*==============================================================*/
/* Table: Function                                              */
/*==============================================================*/
CREATE TABLE `Function` (
    function_id INTEGER NOT NULL AUTO_INCREMENT,
    function_label VARCHAR(250) NOT NULL,
    CONSTRAINT PK_FUNCTION PRIMARY KEY (function_id)
);

/*==============================================================*/
/* Table: Client                                                */
/*==============================================================*/
CREATE TABLE `Client` (
    client_id INTEGER NOT NULL AUTO_INCREMENT,
    domain_id INTEGER NOT NULL,
    client_society_label VARCHAR(250) NOT NULL,
    client_type ENUM('Privé', 'Public') NOT NULL,
    client_nature ENUM('Principale', 'Secondaire', 'Ancienne') NOT NULL,
    client_address VARCHAR(250) NOT NULL,
    client_phone_number VARCHAR(10) NOT NULL,
    client_revenus INTEGER NOT NULL,
    client_worker_count INTEGER NOT NULL,
    general_comment VARCHAR(250) NULL,
    CONSTRAINT PK_CLIENT PRIMARY KEY (client_id),
    CONSTRAINT FK_CLIENT_DOMAIN FOREIGN KEY (domain_id)
        REFERENCES `Domain` (domain_id)
);

/*==============================================================*/
/* Table: Worker                                                */
/*==============================================================*/
CREATE TABLE `Worker` (
    worker_id INTEGER NOT NULL AUTO_INCREMENT,
    worker_full_name VARCHAR(250) NOT NULL,
    worker_sex ENUM('F', 'M') NOT NULL,
    worker_civility ENUM('Madame', 'Mademoiselle', 'Monsieur') NOT NULL, 
    worker_address1 VARCHAR(250) NOT NULL,
    worker_address2 VARCHAR(250) NULL,
    worker_city VARCHAR(250) NOT NULL,
    worker_postalcode VARCHAR(10) NOT NULL,
    worker_phone_number CHAR(10) NOT NULL,
    CONSTRAINT PK_WORKER PRIMARY KEY (worker_id),
           -- verification si le sex correspond avec civilité
    CONSTRAINT CHK_CIVILITY_WORKER check ((worker_sex like 'F' and worker_civility in ('Madame','Mademoiselle')) 
                                        or (worker_sex like 'M' and worker_civility like 'Monsieur'))
);

/*==============================================================*/
/* Table: Status                                                */
/*==============================================================*/
CREATE TABLE `Status` (
    status_id CHAR(3) NOT NULL,
    status_label VARCHAR(250) NOT NULL,
    CONSTRAINT PK_STATUS PRIMARY KEY (status_id)
);

/*==============================================================*/
/* Table: Contract                                              */
/*==============================================================*/
CREATE TABLE `Contract` (
    contract_id INTEGER NOT NULL AUTO_INCREMENT,
    status_id CHAR(3) NOT NULL,
    function_id INTEGER NOT NULL,
    worker_id INTEGER NOT NULL,
    contract_start_date DATE NOT NULL,
    contract_end_date DATE NULL,
    worker_salary DECIMAL NOT NULL,
    CONSTRAINT PK_CONTRACT PRIMARY KEY (contract_id),
    CONSTRAINT FK_CONTRACT_STATUS FOREIGN KEY (status_id)
        REFERENCES `Status` (status_id),
    CONSTRAINT FK_CONTRACT_FUNCTION FOREIGN KEY (function_id)
        REFERENCES `Function` (function_id),
    CONSTRAINT FK_CONTRACT_WORKER FOREIGN KEY (worker_id)
        REFERENCES `Worker` (worker_id)
);

/*==============================================================*/
/* Table: Document                                              */
/*==============================================================*/
CREATE TABLE `Document` (
    document_id INTEGER NOT NULL AUTO_INCREMENT,
    worker_id INTEGER NOT NULL,
    document_title VARCHAR(250) NOT NULL,
    document_summary VARCHAR(250) NOT NULL,
    document_release_date DATE NOT NULL,
    CONSTRAINT PK_DOCUMENT PRIMARY KEY (document_id),
    CONSTRAINT FK_DOCUMENT_WORKER FOREIGN KEY (worker_id)
        REFERENCES `Worker` (worker_id)
);

/*==============================================================*/
/* Table: External_worker                                       */
/*==============================================================*/
CREATE TABLE `External_worker` (
    external_worker_id INTEGER NOT NULL AUTO_INCREMENT,
    client_id INTEGER NOT NULL,
    function_id INTEGER NULL,
    external_worker_fullname VARCHAR(250) NOT NULL,
    external_worker_sex ENUM('F', 'M') NOT NULL,
    external_worker_civility ENUM('Madame', 'Mademoiselle', 'Monsieur') NOT NULL,
    external_worker_phone_number CHAR(10) NOT NULL,
    CONSTRAINT PK_EXTERNALWORKER PRIMARY KEY (external_worker_id),
    CONSTRAINT FK_EXTERNALWORKER_CLIENT FOREIGN KEY (client_id)
        REFERENCES `Client` (client_id),
    CONSTRAINT FK_EXTERNALWORKER_FUNCTION FOREIGN KEY (function_id)
        REFERENCES `Function` (function_id),
        -- verification si le sex correspond avec civilité
	CONSTRAINT CHK_CIVILITY_EXTERNAL_WORKER check ((external_worker_sex like 'F' and external_worker_civility like 'Madame') 
													or (external_worker_sex like 'F' and external_worker_civility like 'Mademoiselle') 
														or (external_worker_sex like 'M' and external_worker_civility like 'Monsieur'))
);

/*==============================================================*/
/* Table: Sector                                                */
/*==============================================================*/
CREATE TABLE `Sector` (
    sector_id INTEGER NOT NULL AUTO_INCREMENT,
    sector_label VARCHAR(250) NOT NULL,
    CONSTRAINT PK_SECTOR PRIMARY KEY (sector_id)
);

/*==============================================================*/
/* Table: Project                                               */
/*==============================================================*/
CREATE TABLE `Project` (
    project_id CHAR(4) NOT NULL,
    client_id INTEGER NOT NULL,
    external_worker_id INTEGER NULL,	-- contact de client
    sector_id INTEGER NOT NULL,
    worker_id INTEGER NOT NULL,			-- chef de projet
    project_short_label CHAR(10) NOT NULL,
    project_label VARCHAR(250) NOT NULL,
    project_type ENUM('Forfait', 'Régie', 'Assistance') NOT NULL,
    project_planed_start_date DATE NOT NULL,
    project_planed_end_date DATE NOT NULL,
    max_worker_count INTEGER NULL,
    project_estimated_workload INTEGER NULL,
    project_workload_comment VARCHAR(250) NULL,
    project_code_length INTEGER NULL,
    project_client_comment VARCHAR(250) NULL,
    CONSTRAINT PK_PROJECT PRIMARY KEY (project_id),
    CONSTRAINT FK_PROJECT_CLIENT FOREIGN KEY (client_id)
        REFERENCES `Client` (client_id),
    CONSTRAINT FK_PROJECT_EXTERNALWORKER FOREIGN KEY (external_worker_id)
        REFERENCES `External_worker` (external_worker_id),
    CONSTRAINT FK_PROJECT_SECTOR FOREIGN KEY (sector_id)
        REFERENCES `Sector` (sector_id),
    CONSTRAINT FK_PROJECT_WORKER FOREIGN KEY (worker_id)
        REFERENCES `Worker` (worker_id)
);

/*==============================================================*/
/* Table: Stage_type                                            */
/*==============================================================*/
CREATE TABLE `Stage_type` (
    stage_type_id INTEGER NOT NULL AUTO_INCREMENT,
    stage_type_label VARCHAR(250) NOT NULL,
    CONSTRAINT PK_STAGETYPE PRIMARY KEY (stage_type_id)
);

/*==============================================================*/
/* Table: Stage                                                 */
/*==============================================================*/
CREATE TABLE `Stage` (
    stage_id int NOT NULL auto_increment,
    lot CHAR(4) NOT NULL,
    project_id CHAR(4) NOT NULL,
    stage_type_id INTEGER NOT NULL,
    stage_validation_workload INTEGER NULL,
    stage_planned_workload INTEGER NOT NULL,
    CONSTRAINT PK_STAGE PRIMARY KEY (stage_id),
    CONSTRAINT FK_STAGE_PROJECT FOREIGN KEY (project_id)
        REFERENCES `Project` (project_id) on delete cascade,		-- suppression du projet entraine suppression des étapes liés
    CONSTRAINT FK_STAGE_STAGETYPE FOREIGN KEY (stage_type_id)
        REFERENCES `Stage_type` (stage_type_id)
);

/*=================================================================================*/
/* Function 2: Incrementation lot d'étape			 							   */
/*=================================================================================*/

drop function if exists lot_incrementation;

DELIMITER $$

create function lot_incrementation(project_id CHAR(4))
returns char(4)
comment 'Procedure de creation du identifiant pour Entité stage en concatenant 
2 chiffres de numéro de pojet et 2 chiffres en auto incrementation'
reads sql data 
begin 
	declare answer CHAR(4);
	declare substr_project_id char(2);
    declare counter char(2);
    -- 2 chiffres de numéro de projet 
	set substr_project_id = substr(project_id,3);
	-- recherche de numéro de projet 
	SELECT 
		coalesce(SUBSTR(MAX(`Stage`.lot), 3),0)
	FROM `Stage`
	WHERE SUBSTR(`Stage`.lot, 1, 2) LIKE substr_project_id INTO counter;
    -- incrementation plus 1
	set counter = counter + 1;
    -- auto completion jusqu'à 2 chiffres
	SELECT LPAD(counter, 2, '0') INTO counter;
    -- concatenation des deux nombres
	SELECT CONCAT(substr_project_id, counter) INTO answer;
	-- affichage
    return answer;
end$$

DELIMITER ;

/*================================================================================*/
/* Trigger d'implémentation de procédure d'incrémentation lot d'étape			  */
/*================================================================================*/
drop trigger if exists trigger_lot_incrementation;

DELIMITER //

create trigger trigger_lot_incrementation
before insert 
on `Stage` for each row
begin
    set new.lot = lot_incrementation(new.project_id);
end//

DELIMITER ;
/*==============================================================*/
/* Table: Intervention                                          */
/*==============================================================*/
CREATE TABLE `Intervention` (
    intervention_id INTEGER NOT NULL AUTO_INCREMENT,
    stage_id INT NOT NULL,
    activity_id INTEGER NOT NULL,
    worker_id INTEGER NULL,
    external_worker_id INTEGER NULL,
    function_id INTEGER NOT NULL,
    intervention_date DATE NOT NULL,
    intervention_workload INTEGER NULL,
    CONSTRAINT PK_INTERVENTION PRIMARY KEY (intervention_id),
    CONSTRAINT FK_INTERVENTION_STAGE FOREIGN KEY (stage_id)
        REFERENCES `Stage` (stage_id) on delete cascade,			-- suppression d'une étape entraine suppression des interventons liés
    CONSTRAINT FK_INTERVENTION_ACTIVITY FOREIGN KEY (activity_id)
        REFERENCES `Activity` (activity_id),
    CONSTRAINT FK_INTERVENTION_WORKER FOREIGN KEY (worker_id)
        REFERENCES `Worker` (worker_id),
    CONSTRAINT FK_INTERVENTION_EXTERNALWORKER FOREIGN KEY (external_worker_id)
        REFERENCES `External_worker` (external_worker_id),
    CONSTRAINT FK_INTERVENTION_FUNCTION FOREIGN KEY (function_id)
        REFERENCES `Function` (function_id)
);

/*==============================================================*/
/* Table: Permission                                            */
/*==============================================================*/
CREATE TABLE `Permission` (
   `key` VARCHAR(50) NOT NULL,
   description TEXT NOT NULL,
    CONSTRAINT PK_PERMISSION PRIMARY KEY (`key`) 
);

/*==============================================================*/
/* Table: Role                                                  */
/*==============================================================*/
CREATE TABLE `Role` (
    role_id INTEGER NOT NULL AUTO_INCREMENT,
    role_label VARCHAR(250) NOT NULL,
    CONSTRAINT PK_ROLE PRIMARY KEY (role_id)
);

/*==============================================================*/
/* Table: Role_permission                                       */
/*==============================================================*/
CREATE TABLE `Role_permission`(
    role_id INTEGER NOT NULL,
    `key` VARCHAR(50) NOT NULL,
    CONSTRAINT PK_ROLEPERMISSION PRIMARY KEY (role_id,`key`) 
);

/*==============================================================*/
/* Table: User                                                  */
/*==============================================================*/
CREATE TABLE `User` (
    user_id INTEGER NOT NULL AUTO_INCREMENT,
    worker_id INTEGER NOT NULL,
    `login` VARCHAR(30) UNIQUE NOT NULL,
    role_id INTEGER NOT NULL,
    `password` VARCHAR(30) NOT NULL,
    CONSTRAINT PK_USER PRIMARY KEY (user_id),
    CONSTRAINT FK_USER_ROLE FOREIGN KEY (role_id)
        REFERENCES `Role` (role_id),
    CONSTRAINT FK_USER_WORKER FOREIGN KEY (worker_id)
        REFERENCES `Worker` (worker_id)
);

/*==============================================================*/
/* Table: Technical_informaton                                  */
/*==============================================================*/
CREATE TABLE `Technical_information` (
    technical_info_id INTEGER NOT NULL AUTO_INCREMENT,
    technical_info_label VARCHAR(250) NOT NULL,
    CONSTRAINT PK_TECHNICALINFORMATION PRIMARY KEY (technical_info_id)
);

/*==============================================================*/
/* Table: Project_document                                      */
/*==============================================================*/
CREATE TABLE `Project_document` (
    document_id INTEGER NOT NULL,
    project_id CHAR(4) NOT NULL,
    CONSTRAINT PK_PROJECTDOCUMENT PRIMARY KEY (document_id , project_id),
    CONSTRAINT FK_PROJECTDOCUMENT_DOCUMENT FOREIGN KEY (document_id)
        REFERENCES `Document` (document_id),
    CONSTRAINT FK_PROJECTDOCUMENT_PROJECT FOREIGN KEY (project_id)
        REFERENCES `Project` (project_id) on delete cascade 			-- suppression du projet entraine suppression des documents liés
);

/*==============================================================*/
/* Table: Stage_document                                        */
/*==============================================================*/
CREATE TABLE `Stage_document` (
    document_id INTEGER NOT NULL,
    stage_id INT NOT NULL,
    CONSTRAINT PK_STAGEDOCUMENT PRIMARY KEY (document_id , stage_id),
    CONSTRAINT FK_STAGEDOCUMENT_DOCUMENT FOREIGN KEY (document_id)
        REFERENCES `Document` (document_id),
    CONSTRAINT FK_STAGEDOCUMENT_STAGE FOREIGN KEY (stage_id)
        REFERENCES `Stage` (stage_id) on delete cascade 			-- suppression de lien étape - document au moment de suppression de l'étape
);

/*==============================================================*/
/* Table: Intervention_document                                 */
/*==============================================================*/
CREATE TABLE `Intervention_document` (
    document_id INTEGER NOT NULL,
    intervention_id INTEGER NOT NULL,
    CONSTRAINT PK_INTERVENTIONDOCUMENT PRIMARY KEY (document_id , intervention_id),
    CONSTRAINT FK_INTERVENTIONDOCUMENT_DOCUMENT FOREIGN KEY (document_id)
        REFERENCES `Document` (document_id),
    CONSTRAINT FK_INTERVENTIONDOCUMENT_INTERVENTION FOREIGN KEY (intervention_id)
        REFERENCES `Intervention` (intervention_id) on delete cascade			-- suppression de lien intervention - document au moment de suppression d'intervention
);

/*==============================================================*/
/* Table: Project_technical_information                         */
/*==============================================================*/
CREATE TABLE `Project_technical_information` (
    technical_info_id INTEGER NOT NULL,
    project_id CHAR(4) NOT NULL,
    CONSTRAINT PK_PROJECTTECHNICALINFORMATION PRIMARY KEY (technical_info_id , project_id),
    CONSTRAINT FK_PROJECTTECHNICALINFORMATION_TECHNICALINFORMATION FOREIGN KEY (technical_info_id)
        REFERENCES `Technical_information` (technical_info_id),
    CONSTRAINT FK_PROJECTTECHNICALINFORMATION_PROJECT FOREIGN KEY (project_id)
        REFERENCES `Project` (project_id) on delete cascade 			-- suppression de lien projet - information technique au moment de suppression du projet
);

/*==============================================================*/
/* Table: Intervention_technical_information                    */
/*==============================================================*/
CREATE TABLE `Intervention_technical_information` (
    technical_info_id INTEGER NOT NULL,
    intervention_id INTEGER NOT NULL,
    CONSTRAINT PK_INTERVENTIONTECHNICALINFORMATION PRIMARY KEY (technical_info_id , intervention_id),
    CONSTRAINT FK_INTERVENTIONTECHNICALINFORMATION_TECHNICALINFORMATION FOREIGN KEY (technical_info_id)
        REFERENCES `Technical_information` (technical_info_id),
    CONSTRAINT FK_INTERVENTIONTECHNICALINFORMATION_INTERVENTION FOREIGN KEY (intervention_id)
        REFERENCES `Intervention` (intervention_id) on delete cascade 		-- suppression de lien intervention - information technique au moment de suppression d'intervention
);


