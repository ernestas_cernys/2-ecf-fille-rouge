/*==============================================================*/
/* Project: Fil Rouge « Active »                                */
/* Author: Ernestas Cernys										*/
/* Version: final							Date: 14/02/2020	*/
/* 																*/
/*==============================================================*/

/*==============================================================*/
/* 							Querys			                    */
/*==============================================================*/

/*=========================================================================================================*/
/* Select query 1: On souhaite obtenir par secteur d’activité la moyenne des charges estimées des projets  */
/*=========================================================================================================*/
SELECT 
    sector_label AS `Secteur d'activité`,
    truncate(AVG(project_estimated_workload),0) AS `Moyenne des charges estimées`
FROM
    `Project` p
        JOIN
    `Sector` s ON p.sector_id = s.sector_id
GROUP BY p.sector_id
ORDER BY `Moyenne des charges estimées`;

/*==================================================================================================*/
/* Select query 2:	On souhaite obtenir la liste des projets (libellé court) 						*/
/* sur lesquels un collaborateur est intervenu avec sa fonction dans les projets.  					*/
/*==================================================================================================*/
SELECT DISTINCT
    worker_full_name AS 'Collaborateur',
    project_short_label AS 'Projet',
    function_label AS 'Fonction'
FROM
    `Intervention` i
        JOIN
    `Stage` s ON i.stage_id = s.stage_id
        JOIN
    `Project` p ON s.project_id = p.project_id
        JOIN
    `Function` f ON i.function_id = f.function_id
        JOIN
    `Worker` w ON i.worker_id = w.worker_id
ORDER BY worker_full_name , project_short_label , function_label;

/*==================================================================================================*/
/* Select query 3:	On souhaite obtenir à la date du jour la liste des projets en cours, 			*/
/* par secteur d’activité avec le nombre de collaborateurs associés aux projets par fonction   		*/
/*==================================================================================================*/
SELECT 
    project_short_label AS 'Projet',
    sector_label AS 'Secteur',
    COUNT(i.worker_id) AS `Nombre de collaborateurs associés`,
    function_label AS 'Fonction'
FROM
    `Intervention` i
        JOIN
    `Stage` st ON i.stage_id = st.stage_id
        JOIN
    `Project` p ON st.project_id = p.project_id
        JOIN
    `Sector` s ON p.sector_id = s.sector_id
        JOIN
    `Function` f ON i.function_id = f.function_id
WHERE
    stage_validation_workload IS NULL
GROUP BY f.function_id , p.project_id;		-- groupement par function et projet 

/*==========================================================================================================*/
/* Delete query 1:	Supprimer les projets terminés et qui n’ont pas eut de charges (étapes) associées.  	*/
/* Projet est consideré comme terminé quand tous les étapes sont validés  									*/
/*==========================================================================================================*/
DELETE FROM `Project` USING `Project`
        LEFT JOIN
    `Stage` ON `Project`.project_id = `Stage`.project_id 
WHERE
    `Stage`.project_id IS NULL				-- suppression des projets sans étapes attribués
    OR `Project`.project_id IN (SELECT 		-- suppression des projets terminés
        project_id
    FROM
        `Stage`
    WHERE
        stage_validation_workload IS NOT NULL
    GROUP BY project_id);

/*==========================================================================================================*/
/* Update query 1: Augmenter les salaires des collaborateurs de : 5% si ils ont plus de 5 ans d’ancienneté	*/
/*==========================================================================================================*/
UPDATE `Contract` 
SET 
    worker_salary = worker_salary * 1.05
WHERE
    contract_id IN (SELECT * FROM				-- recherche de dernier contrat par collaborateur
            (SELECT 
                MAX(contract_id)
            FROM
                `Contract`
            GROUP BY worker_id) AS last_contract)
        AND worker_id IN (SELECT * FROM			-- recherche si collaborateur a 5 ans d'ancienneté
            (SELECT 
                worker_id
            FROM
                `Contract`
            GROUP BY worker_id
            /* somme de durée des contrats pour calculer anciennité 
            (si contrat  n'est pas terminé calcul avec date de jour) */
            HAVING SUM(DATEDIFF(COALESCE(contract_end_date, CURDATE()), contract_start_date))   
            -- calcul plus précis d'interval d'anciennité 
            > /* 365 * 5 */ (select datediff(curdate(),curdate() - interval 5 year))) AS old_timers)
        -- recherche si collaborateur travaille toujours (son dernier contrat n'a pas de date de fin)
        AND contract_end_date IS NULL;			

/*==========================================================================================================*/
/* Trigger 1: Vérifier que la date prévisionnelle de début du projet est inférieure ou égale la date de fin */
/*==========================================================================================================*/

-- Creation de procedure commune pour triggers de insert et update 
drop procedure if exists check_planned_dates;

DELIMITER //

create procedure check_planned_dates (in start_date date, in end_date date)
begin
	if (start_date > end_date) then
    SIGNAL SQLSTATE '45000' 
	SET MESSAGE_TEXT = 'La date de début du projet et supérieure à la date de fin du projet';
    end if;
end// 

DELIMITER ;

-- Trigger des inserts
drop trigger if exists trigger_insert_planned_dates;

DELIMITER //

create trigger trigger_insert_planned_dates
before insert 
on `Project` for each row
begin
	call check_planned_dates(new.project_planed_start_date, new.project_planed_end_date);
end//

DELIMITER ;

-- Trigger des updates 
drop trigger if exists trigger_update_planned_dates;

DELIMITER //

create trigger trigger_update_planned_dates
before update 
on `Project` for each row
begin
	call check_planned_dates(new.project_planed_start_date, new.project_planed_end_date);
end//

DELIMITER ;

-- UPDATE `fil_rouge`.`project` SET `project_planed_end_date` = '2010-09-20' WHERE (`project_id` = '1901');
/* INSERT INTO `fil_rouge`.`project` (`project_id`, `client_id`, `external_worker_id`, `sector_id`, `worker_id`, 
`project_short_label`, `project_label`, `project_type`, `project_planed_start_date`, `project_planed_end_date`,
 `max_worker_count`, `project_estimated_workload`) VALUES ('1913', '1', '1', '1', '1', 'fds', 'fsdf', 'Régie',
  '2019-10-15', '2010-02-15', '45', '15646'); */


/*==========================================================================================================*/
/* Trigger 2:  Vérifier chiffre d’affaire du client, si supérieur à 1 million d’euros par personne 			*/
/*==========================================================================================================*/
-- Procedure commune de verification 
drop procedure if exists check_chiffre_affaire;

DELIMITER //

create procedure check_chiffre_affaire(in revenus int, in workers int)
begin
	if ((revenus / workers) > 1000000) then
    SIGNAL SQLSTATE '45000' 
	SET MESSAGE_TEXT = 'Le Chiffre d’affaire du client ne peut pas être soupérrieur au 1 million par personne';
    end if;
end// 

DELIMITER ;

-- Trigger d'insert 
drop trigger if exists trigger_insert_chiffre_affaire;

DELIMITER //

create trigger trigger_insert_chiffre_affaire
before insert 
on `Client` for each row
begin
	call check_chiffre_affaire(new.client_revenus, new.client_worker_count);
end// 

DELIMITER ;

-- Trigger d'update
drop trigger if exists trigger_update_chiffre_affaire;

DELIMITER //

create trigger trigger_update_chiffre_affaire
before update
on `Client` for each row
begin
	call check_chiffre_affaire(new.client_revenus, new.client_worker_count);
end//

DELIMITER ;

-- UPDATE `fil_rouge`.`client` SET `client_revenus` = '4265300', `client_worker_count` = '1' WHERE (`client_id` = '1');
/* INSERT INTO `fil_rouge`.`client` (`client_id`, `domain_id`, `client_society_label`, `client_type`, `client_nature`,
 `client_address`, `client_phone_number`, `client_revenus`, `client_worker_count`) VALUES ('11', '1', 'FDLSJ', 'Privé',
  'Principale', '3 Appartement 2 Rue de Kleber', '624589784', '1000001', '1'); */

/*==========================================================================================================*/
/* Trigger 3:  Vérifier la cohérence du code statut  														*/
/* Passages possibles de : - S (stagiaire) à D (CDD) ou I (CDI), - D (CDD) à I (CDI)						*/
/*==========================================================================================================*/
drop trigger if exists trigger_update_status;

DELIMITER //

create trigger trigger_update_status
before update
on `Contract` for each row
begin
	CASE
    WHEN old.status_id like 'STA' AND new.status_id not in ('CDD','CDI') THEN 
		SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = 'Statut de stagiaire peut evoluer que vers CDD ou CDI'; 
    WHEN old.status_id like 'CDD' AND new.status_id not in ('CDI') THEN 
		SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = 'Statut de CDD peut evoluer que vers CDI'; 
	WHEN old.status_id like 'CDI' AND new.status_id not in ('CDI') THEN
		SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = 'Statut de CDI ne peut pas evoluer vers Stagiaire ou CDD'; 
    ELSE SET new.status_id = new.status_id;
    end case;
    /* s'il va avoir des nouvels statuts dans le temps c'est facile de rajouter 
    des WHEN pour autres verifications et valeurs dans 'in' */
end//

DELIMITER ;

/**
UPDATE `fil_rouge`.`contract` SET `status_id` = 'CDD' WHERE (`contract_id` = '1');
UPDATE `fil_rouge`.`contract` SET `status_id` = 'CDI' WHERE (`contract_id` = '4');
UPDATE `fil_rouge`.`contract` SET `status_id` = 'CDD' WHERE (`contract_id` = '3');
UPDATE `fil_rouge`.`contract` SET `status_id` = 'STA' WHERE (`contract_id` = '7');
*/

/*==========================================================================================================*/
/* Trigger 4: Ne pas supprimer un projet si la date réelle de fin est inférieure à 2 mois de la date du jour*/
/*==========================================================================================================*/
drop trigger if exists trigger_delete_project;

DELIMITER //

create trigger trigger_delete_project
before delete
on `Project` for each row
begin
	if (old.project_id in (
		select distinct p.project_id
		from `Project` p
		join `Stage` s on p.project_id = s.project_id 
		join `Intervention` i on s.stage_id = i.stage_id 
		/* where datediff(CURDATE(), intervention_date) < 62) */				
		where intervention_date > (select curdate() - interval 2 month)))		-- calcule plus precise de 2 mois
	then 
        SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = 'Interdiction de suppression de projets avec date de fin inférieure à 2 mois par rapport à la date du jour'; 
	end if;
end//

DELIMITER ;

-- DELETE FROM `fil_rouge`.`project` WHERE (`project_id` = '1908');
