/*==============================================================*/
/* Project: Fil Rouge « Active »                                */
/* Author: Ernestas Cernys										*/
/* Version: final							Date: 14/02/2020	*/
/* 																*/
/*==============================================================*/

/*==============================================================*/
/* 					Procedures et fonctions	                    */
/*==============================================================*/

/*==============================================================*/
/* Function 1: Incrementation d'identifiant du projet 			*/
/*==============================================================*/
drop function if exists project_id_incrementation;

DELIMITER $$

create function project_id_incrementation()
returns char(4)
comment 'Procedure de creation du identifiant pour Entité projet en concatenant 
2 chiffres de l\'année actuelle et 2 chiffres en auto incrementation par ans'
reads sql data 
begin 
	declare answer CHAR(4);
	declare substr_year int;
    declare yearly_number char(2);
	-- recherche de numéro plus grand de l'année actuelle 
SELECT 
    coalesce((MAX(`Project`.project_id), 3),0)
FROM
    `Project`
WHERE
    SUBSTR(`Project`.project_id, 1, 2) LIKE SUBSTR(YEAR(CURDATE()), 3) INTO yearly_number;

    -- incrementation plus 1
    set yearly_number = yearly_number + 1;
    -- auto completion jusqu'à 2 chiffres
SELECT LPAD(yearly_number, 2, '0') INTO yearly_number;
    -- 2 chiffres de l'année actuelle
	set substr_year = substr(year(curdate()),3);
    -- concatenation des deux nombres
	SELECT CONCAT(substr_year, yearly_number) INTO answer;
	-- affichage
    return answer;
end$$

DELIMITER ;

/*================================================================================*/
/* Trigger d'implémentation de procédure d'incrémentation d'identifiant du projet */
/*================================================================================*/
drop trigger if exists trigger_project_id_incrementation;

DELIMITER //

create trigger trigger_project_id_incrementation
before insert 
on `Project` for each row
begin
    set project_id = project_id_incrementation();
end//

DELIMITER ;

/*=================================================================================*/
/* Function 2: Incrementation lot d'étape			 							   */
/*=================================================================================*/

drop function if exists lot_incrementation;

DELIMITER $$

create function lot_incrementation(project_id CHAR(4))
returns char(4)
comment 'Procedure de creation du identifiant pour Entité stage en concatenant 
2 chiffres de numéro de pojet et 2 chiffres en auto incrementation'
reads sql data 
begin 
	declare answer CHAR(4);
	declare substr_project_id char(2);
    declare counter char(2);
    -- 2 chiffres de numéro de projet 
	set substr_project_id = substr(project_id,3);
	-- recherche de numéro de projet 
	SELECT 
		coalesce(SUBSTR(MAX(`Stage`.lot), 3),0)
	FROM `Stage`
	WHERE SUBSTR(`Stage`.lot, 1, 2) LIKE substr_project_id INTO counter;
    -- incrementation plus 1
	set counter = counter + 1;
    -- auto completion jusqu'à 2 chiffres
	SELECT LPAD(counter, 2, '0') INTO counter;
    -- concatenation des deux nombres
	SELECT CONCAT(substr_project_id, counter) INTO answer;
	-- affichage
    return answer;
end$$

DELIMITER ;

/*================================================================================*/
/* Trigger d'implémentation de procédure d'incrémentation lot d'étape			  */
/*================================================================================*/
drop trigger if exists trigger_lot_incrementation;

DELIMITER //

create trigger trigger_lot_incrementation
before insert 
on `Stage` for each row
begin
    set new.lot = lot_incrementation(new.project_id);
end//

DELIMITER ;



/*=================================================================================*/
/* Procedure 1: Calcul de la moyenne des charges estimées sur les projets en cours */
/* Projet terminé = projet avec tous les étapes validées						   */
/*=================================================================================*/
drop procedure if exists current_project_workload;

DELIMITER $$

create procedure current_project_workload()
comment 'Procédure d\'obtention de la moyenne des charges estimées sur les projets en cours'
begin 
	select truncate(avg(project_estimated_workload),0) as 'Moyenne de charges de projets en cours'
    from `Project` p
    join `Stage` s on p.project_id = s.project_id 
    where stage_validation_workload IS NULL;
end$$

DELIMITER ;

call current_project_workload();

/*==============================================================================================================*/
/* Procedure 2: Obtention d'une liste des projets associés au thème technique et terminés depuis moins de 2 ans */
/* Projet terminé = projet avec tous les étapes validés						   									*/
/* Recherche par technologies associées par Responsable d'étude directement au projet							*/
/*==============================================================================================================*/
drop procedure if exists recent_projects_by_project_tech_info;

DELIMITER $$

create procedure recent_projects_by_project_tech_info(in tech_info varchar(250))
comment 'Procédure d\'obtention de la liste des projets associés au thème technique et terminés depuis moins de 2 ans'
begin 
	select distinct p.project_id, tech_info
    from `Project` p
    join `Project_technical_information` pti on p.project_id = pti.project_id
    join `Technical_information` ti on pti.technical_info_id = ti.technical_info_id
    join `Stage` s on p.project_id = s.project_id
    join `Intervention` i on s.stage_id = i.stage_id
    -- recherche de technical_information 
    where ti.technical_info_label like tech_info 
    -- recherche si date de fin réelle du projet est infèrieur de 2 ans  
    and intervention_date
    > /* 365*2 */ (select curdate() - interval 2 year)
	-- recherche que dans projets terminés = toutes les étapes validés
    and s.project_id not in (								
		select distinct project_id 
        from `Stage` 
        where stage_validation_workload is null
        );
end$$

DELIMITER ;

call recent_projects_by_project_tech_info('JAVA');

/*==============================================================================================================*/
/* Procedure 2: Obtention d'une liste des projets associés au thème technique et terminés depuis moins de 2 ans */
/* Projet terminé = projet avec tous les étapes validés						   									*/
/* Recherche par technologies associées aux interventions 														*/
/*==============================================================================================================*/
drop procedure if exists recent_projects_by_intervention_tech_info;

DELIMITER $$

create procedure recent_projects_by_intervention_tech_info(in tech_info varchar(250))
comment 'Procédure d\'obtention de la liste des projets associés au thème technique et terminés depuis moins de 2 ans'
begin 
	select distinct s.project_id, tech_info
    from `Stage` s
    join `Intervention` i on s.stage_id = i.stage_id
    join `Intervention_technical_information` iti on i.intervention_id = iti.intervention_id
    join `Technical_information` ti on iti.technical_info_id = ti.technical_info_id
    
    -- recherche de technical_information 
    where ti.technical_info_label like tech_info 
    -- recherche si date de fin réelle du projet est inférieur de 2 ans  
    and intervention_date
    > /* 365*2 */ (select curdate() - interval 2 year)
	-- recherche que dans projets terminés = toutes les étapes validées
    and s.project_id not in (								
		select distinct project_id 
        from `Stage` 
        where stage_validation_workload is null
        );
end$$

DELIMITER ;

call recent_projects_by_intervention_tech_info('JAVA');

/*============================================================================================================*/
/* Procedure 3: Obtention d'une liste des interventions des collaborateurs sur un projet entre deux dates     */
/* Le nom du collaborateur, la fonction en clair du collaborateur,les dates début et fin, l'activité associée */
/*============================================================================================================*/
drop procedure if exists search_interventions_by_date;

DELIMITER $$

create procedure search_interventions_by_date(in date1 date, in date2 date, in projectid int)
comment 'Procédure d\'obtention de la liste des interventions des collaborateurs sur un projet entre deux dates'
begin 
	declare start_date date;
    declare end_date date;
    -- vérification date de début et de fin
    if (date1 > date2) then
		set start_date = date2;
        set end_date = date1;
	else 
		set start_date = date1;
        set end_date = date2;
	end if;
    -- recherche des interventions
    select projectid, worker_full_name, external_worker_fullname, function_label, intervention_date, activity_label
    from `Intervention` i
    join `Stage` s on i.stage_id = s.stage_id
    left join `Worker` w on i.worker_id = w.worker_id
    left join `External_worker` ew on i.external_worker_id = ew.external_worker_id
    join `Function` f on i.function_id = f.function_id
    join `Activity` a on i.activity_id = a.activity_id
    where s.project_id = projectid 
		and intervention_date between start_date and end_date
	order by intervention_date;
end$$

DELIMITER ;

call search_interventions_by_date('2019-01-01','2019-12-31','1901');